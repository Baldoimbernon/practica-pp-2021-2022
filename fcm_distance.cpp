#include "definitions.h"

type_data euclidean_distance (type_data *a, type_data *b, unsigned int columns, int m1, int m2)
{
	unsigned int i;
	type_data total = 0;
	type_data temp_op;
	for (i = 0; i < columns; i++)
	{
		total += pow((*(a+i) - *(b+i)),2);
	}
  return(sqrt(total));
}
