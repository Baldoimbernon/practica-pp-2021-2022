#ifndef COMMON_H_
#define COMMON_H_
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <math.h>
#include <time.h>
#include <ostream>
#include <iomanip>
#include "definitions.h"

// includes, project

extern char *extract_name (char *);
extern void exam_data (char *, unsigned int *, unsigned int *, char);
//extern type_data *read_data (char *, unsigned int, unsigned int, type_data *, char);
extern void create_u (unsigned int ,unsigned int, type_data *&, type_data);
extern void create_static_u (type_data *&m);
extern type_data *create_obj (type_data *, unsigned int);
extern void create_tmp_dist (type_data *, unsigned int, unsigned int);
//extern type_data *new_data (type_data *, type_data *, type_data *, int, int, int, int);
extern void multiply_matrix_one_one (type_data *, type_data *, type_data *, unsigned int, unsigned int, unsigned int);
extern void u_up (type_data *, type_data *, unsigned int, unsigned int, type_data);
extern void transposed (type_data *, type_data *, unsigned int, unsigned int);
extern void denominator_center(type_data *, type_data *, unsigned int, unsigned int, unsigned int);
extern void multiply (type_data *, type_data *, type_data *, unsigned int, unsigned int, unsigned int);
extern void distance_matrix (type_data *,type_data *, type_data *, unsigned int, unsigned int, unsigned int);
extern type_data multiply_one_one(type_data *,type_data *, unsigned int, unsigned int);
extern void temp_dist (type_data *, type_data *, unsigned int, unsigned int, type_data, type_data);
extern void sum_col (type_data *, type_data *, unsigned int, unsigned int);
extern void new_u_for_next_it (type_data *&, type_data *, type_data *, unsigned int, unsigned int);
//auxiliar functions
extern void show_data (type_data *, unsigned int, unsigned int);
extern void show_data_file (type_data *m, type_data *c, unsigned int c_numbers, unsigned int rows, unsigned int columns, char *output);

#endif /* COMMON_H_ */
