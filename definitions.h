#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H
#include "type.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BLOCK_SIZE 512
#define BLOCK_64 64
#define BLOCK_SM 64
#define BLOCK_128 128
#define BLOCK_256 256 


#define WARP_SIZE 32
#define MAX_LINE 16384
#define BLOCKSIZE 1024

#endif
