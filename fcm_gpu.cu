#include <stdio.h>
#include "energy_cuda.h"
#include "fcm_common.h"
#include "wtime.h"
#include "type.h"
#include <cublas_v2.h>

//Devuelve un numero entero aleatorio entre 0 y UINT_MAX

__global__ void u_up_kernel (type_data *mf, type_data *u, type_data exponent, unsigned int max)
{ 
/* ......*/
}

__global__ void	denominator_center_kernel (type_data *den_center, type_data *sum_col_u_2_ele, unsigned int columns, unsigned int max)
{
/* ..... */		
}


extern void fcm_calculation_GPU (type_data *data_matrix,type_data *u, type_data *&u_host, type_data *&center_host,unsigned int rows,unsigned int columns,unsigned int c_numbers, type_data exponent,type_data error,unsigned int mode,unsigned int iterations, int ngpu)
{
   //HAY QUE RESERVAR MEMORIA EN EL HOST Y EL DEVICE ANTES DE HACER USO DE ELLOS.
	 type_data *mf, *center, *matrix_unidad_h, *obj_fcn, total_iteration;
	 float milliseconds, temp_exponent;
	 double time_init, time_fin;

	 unsigned long int bk;
	 unsigned long int th;
	 unsigned long int th_max;

	 curandState_t *states;
	 cudaError_t cudaStatus;

	 cudaEvent_t start, stop, start_bucle, stop_bucle;
	 //EXPONENTE
	 if (exponent != 1) temp_exponent = -2/(exponent-1); else exit(1);
	 matrix_unidad_h = (type_data *)malloc(sizeof(type_data)*rows);
	 for (int i = 0; i < rows; i++) matrix_unidad_h[i] = 1.0f;

	
	 cudaSetDevice(ngpu);
	 cudaDeviceSynchronize();
	 cudaStatus = cudaGetLastError();
	 if (cudaStatus != cudaSuccess) fprintf(stderr, "Selector GPU execution failed \n");


	 cudaEventCreate(&start_bucle);
	 cudaEventCreate(&stop_bucle);
	 cudaEventCreate(&start);
	 cudaEventCreate(&stop);

   cudaEventRecord(start_bucle);
	 //CREAMOS LA MATRIZ U
	 //cudaStatus = cudaMalloc(&u, u_size);
	 //if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc u values failed!");
	 //bk = ceil(rows / BLOCK_256);
	 //dim3 bl_init_u (bk+1);
	 //dim3 th_init_u (BLOCK_256);
	 //th_max = rows;
	 //init_u <<<bl_init_u, th_init_u>>> (states,u,c_numbers,rows,th_max);
   // ...
   
	 cudaEventRecord(stop_bucle);
	 cudaEventSynchronize(stop_bucle);
	 cudaEventElapsedTime(&milliseconds, start_bucle, stop_bucle);
	 printf("TIME create-transfer CPU-GPU: %lf\n", milliseconds/1000);

	for (int step = 0; step < iterations; step++)
	{ 
		//u_up_kernel <<<bl_up_u_kernel, th_up_u_kernel>>> (mf, u, exponent, th_max_up_u_kernel);

		// LLAMADAS A LOS DISTINTOS KERNELS....
    obj_fcn[step] = total_iteration;
		printf("Iteration count GPU= %d, obj_fcn = %f\n", step, obj_fcn[step]);
		//if (step > 0) if (fabs(obj_fcn[step] - obj_fcn[step-1]) < error) break; //QUITADO BALDO
		//exit(0);
	}
	u_host = (type_data *)malloc(c_numbers * rows * sizeof(type_data));
	cudaMemcpy(u_host,u,c_numbers * rows * sizeof(type_data),cudaMemcpyDeviceToHost);
	cudaMemcpy(center_host,center,c_numbers * columns * sizeof(type_data),cudaMemcpyDeviceToHost);

	cudaEventRecord(stop_bucle); 
	cudaEventSynchronize(stop_bucle);
	cudaEventElapsedTime(&milliseconds, start_bucle, stop_bucle);
	printf("TIME bucle eventos: %lf\n", milliseconds/1000);

	time_fin = wtime() - time_init;
	printf("TIME bucle: %lf\n", time_fin);
}


type_data extract_number (char *buffer, int *pos, char separator)
{       int k = 0;
        char c;
        //c = buffer[*pos];
        char temp[100];
        //type_data number;
        while (buffer[*pos] == ' ') (*pos)++;
 	c = buffer[*pos];
	while (c != separator)
	{
		temp[k] = buffer[*pos];
		k++;(*pos)++;
	        c = buffer[*pos];
	}
	(*pos)++;
	temp[k]='\0';
	if (c == '\n') temp[k-1] = '\0';
	//printf("%s\n",temp);
	return ((type_data)atof(temp));
}

extern type_data *read_data (char *input, unsigned int rows, unsigned int columns, type_data *m, char separator)
{
     int pos = 0,i = 0, j = 0;
     //printf("%d %d\n",rows,columns);
     //m = (FLOAT *)malloc(rows*columns*sizeof(FLOAT));
     cudaMallocHost(&m,rows*columns*sizeof(type_data));

     char buffer[MAX_LINE+1];// = (char *)malloc(sizeof(char)*MAX_LINE);
     FILE *fp;
     if ((fp =  fopen(input,"r")) == NULL) {
     	printf("ReadInput(): Can't open file \"%s\"\n", input);
	exit(1);
	}
     while((fgets(buffer,MAX_LINE,fp) != NULL) && (i < rows))
     {
	for (j = 0;j < columns;j++)
	{
		m[i*columns + j] = extract_number(buffer,&pos,separator);
		//printf("%f \n",m[i*columns + j]);
	}
	pos = 0;
	i++;
       //printf("%u %u\n",i,j);
     }
     fclose(fp);
     return (m);
}
