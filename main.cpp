#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "definitions.h"
#include "fcm_common.h"
#include "wtime.h"
#include "fcm_gpu.h"
#include "fcm_cpu.h"
#include "fcm_omp.h"


using namespace std;

int main (int argc, char **argv){

	char c;
	int device, data_print = 0, len, hilos;
	unsigned int c_numbers, rows = 0, columns = 0, mode, iterations;
	type_data exponent, *data_matrix, *u, error, *u_2_ele, *new_data_matrix, *mf, *center, *dist, *obj_fcn, *dist_2, *temp_dist_2_mf, *tmp_dist, *sum_col_dist, *den_center, *den_center_t,*numerator,*mf_t, *u_t, *u_host;
	
	char *data_file, *sep, separator, *out_file;		
	double time_init,time_end,t_f_ini, t_f_end;


	data_file = (char *)malloc(sizeof(char)*100);
	sep = (char *)malloc(sizeof(char)*5);
	separator = ' ';
	mode = 0; //SEQUENTIAL MODE	
	iterations = 100;
	exponent = 2.0f;
	error = 1e-5f;

	while ((c = getopt (argc, argv, "v:r:e:c:f:s:m:n:t:h")) != -1) {
	  switch (c) {
			case 'm':
				mode = atoi(optarg);
				break;
		    	case 'v':
	     			printf("FCM algorithm Calculation v.1.0\n\n");
				return 0;
			case 'c':
				c_numbers = atoi(optarg);
				break;
			case 'f':
				strcpy(data_file,optarg);
				break;
			case 's':
				strcpy(sep,optarg);
				separator = sep[0];
				break;
			case 'e':
				exponent = atof(optarg);
				break;
			case 'i':
				iterations = atoi(optarg);
				break;
			case 'r':
				error = atof(optarg);
				break;
      case 't':
        hilos = atoi(optarg);
        break;
			case 'h':
			case '?':
	      		printf("Usage:\tfcm -c cluster_numbers -f fichero_data [-s \"char_separator\" -m MODE] [-n DEVICE] [-h | -? HELP] \n");
				printf("\t<Params>\n");
		    	printf("\t\t-v\t\tOutput version information and exit\n");		    	
	    		return 0;
    	  }
 	}	
	device = 0;//ngpu;
  //CONFIGURATION SIMULATION
	exam_data (data_file,&rows,&columns,separator); 			
	data_matrix = read_data (data_file,rows,columns,data_matrix,separator);
	
  printf("DATA_MATRIX. rows %d columns %d cluster %d\n\n",rows,columns,c_numbers); 		
	switch (mode) {
		case 0:
			printf("\nFCM ALGORITHM SEQUENTIAL MODE...\n");
      printf("*************************************\n");
			fcm_calculation_CPU (data_matrix,u,center,rows,columns,c_numbers,exponent,error,mode,iterations);
      break; 
		case 1:
			printf("\nFCM ALGORITHM OPEN-MP MODE...\n");
			printf("*************************************\n");
      //fcm_calculation_OMP (data_matrix,u,......);
			break;			
		case 2:
			printf("\nFCM ALGORITHM GPU MODE...\n");
			printf("*************************************\n");
			//fcm_calculation_GPU (data_matrix,u,u_host,....);
			break;
	  	default:
      			printf("Wrong mode type: %d.  Use -h for help.\n", mode);
			return 0;
	}
	//show_data(u_host,c_numbers,rows);
	//exit(0);
	//printf("\nTime: %f seg\n",time_end);
  u_t = (type_data *)malloc(rows*c_numbers*sizeof(type_data));	
	if (mode == 2)
	//u_t = (type_data *)malloc(rows*c_numbers*sizeof(type_data));
       		transposed (u_t,u_host,c_numbers,rows);
	else
		transposed (u_t,u,c_numbers,rows);
	//show_data(u_t,rows,c_numbers);
	//exit(0);
	show_data_file(u_t,center,c_numbers,rows,columns,data_file);
	//}
	//printf("rows %d columns %d cluster %d\n",rows,columns,c_numbers);

	return 0;	

}

