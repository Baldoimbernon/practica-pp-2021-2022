#ifndef _FCM_CPU_H
#define _FCM_CPU_H
#include "type.h"

extern void fcm_calculation_CPU (type_data *data_matrix,type_data *&u, type_data *&center,unsigned int rows,unsigned int columns,unsigned int c_numbers, type_data exponent,type_data error,unsigned int mode,unsigned int iterations);

#endif
