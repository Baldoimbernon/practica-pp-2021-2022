#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include <cuda.h>
//#include <cuda_runtime.h>
#include "definitions.h"
#include "fcm_common.h"
#include "wtime.h"


using namespace std;

extern void fcm_calculation_CPU (type_data *data_matrix,type_data *&u, type_data *&center,unsigned int rows,unsigned int columns, unsigned int c_numbers, type_data exponent,type_data error,unsigned int mode,unsigned int iterations)
{
	type_data *u_2_ele, *new_data_matrix, *mf, *obj_fcn, *dist, *dist_2, *temp_dist_2_mf, *tmp_dist, *sum_col_dist, *den_center, *den_center_t,*numerator,*mf_t, *u_t;

	//u = (type_data *)malloc(c_numbers*rows*sizeof(type_data));
	
	obj_fcn = create_obj (obj_fcn, iterations);
        //CREACION ALEATORIA
	create_u (c_numbers,rows,u,exponent);

	//CREACION ESTATICA IRIS PARA PRUEBAS
	//u = (type_data *)malloc(c_numbers*rows*sizeof(type_data));
	//create_static_u (u);
        //show_data(u,c_numbers,rows);
	//exit(0);
	//printf("\n");

	mf = (type_data *)malloc(c_numbers*rows*sizeof(type_data));
	mf_t = (type_data *)malloc(c_numbers*rows*sizeof(type_data));
	den_center = (type_data *)malloc( sizeof(type_data)*columns*c_numbers);
	den_center_t = (type_data *)malloc( sizeof(type_data)*columns*c_numbers);
	numerator = (type_data *)malloc( sizeof(type_data)*c_numbers*columns); 
	center = (type_data *)malloc( sizeof(type_data)*c_numbers*columns);
	dist = (type_data *)malloc( sizeof(type_data)*c_numbers*rows);
	dist_2 = (type_data *)malloc( sizeof(type_data)*c_numbers*rows);
	tmp_dist = (type_data *)malloc( sizeof(type_data)*c_numbers*rows);
	sum_col_dist = (type_data *)malloc( rows*sizeof(type_data));
	
  for (int step = 0; step < iterations; step++)
  {
		//t_f_ini = wtime();
        	u_up(mf,u,c_numbers,rows,exponent);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime MF: %f seg\n",t_f_end);

		//printf("MF:\n");show_data(mf,c_numbers,rows);
		//t_f_ini = wtime();
		transposed (mf_t,mf,c_numbers,rows);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime Transposed: %f seg\n",t_f_end);
    //printf("MF_t:\n");show_data(mf_t,rows,c_numbers);
		//t_f_ini = wtime();
		denominator_center(den_center,mf_t,rows,columns,c_numbers);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime denominator_center: %f seg\n",t_f_end);
		//t_f_ini = wtime();
		transposed(den_center_t,den_center,columns,c_numbers);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime transposed denominator_center: %f seg\n",t_f_end);
		//show_data(den_center,columns,c_numbers);
		//t_f_ini = wtime();
		//printf("c_numbers %d rows %d columns %d\n",c_numbers,rows,columns);
		multiply (numerator,mf,data_matrix,c_numbers,rows,columns); //BUENO
		//numerator = multiply_GPU (numerator,mf,data_matrix,c_numbers,rows,columns);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime multiply: %f seg\n",t_f_end);
		//printf("NUMERATOR:\n");show_data(numerator,c_numbers,columns);
		//show_data(den_center_t,c_numbers,columns);
		//t_f_ini = wtime();
    multiply_matrix_one_one (center,numerator,den_center_t,c_numbers,rows,columns);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime multiply one-one: %f seg\n",t_f_end);
		//t_f_ini = wtime();
    distance_matrix (dist,center,data_matrix,rows,columns,c_numbers);
		//t_f_end = wtime() - t_f_ini;
		//printf("\nTime dist: %f seg\n",t_f_end);
		//t_f_ini = wtime();
    u_up(dist_2,dist,c_numbers,rows,2);
		//t_f_end = wtime() - t_f_ini;
		//t_f_ini = wtime();
    obj_fcn[step] = multiply_one_one(dist_2,mf,c_numbers,rows);
		//t_f_end = wtime() - t_f_ini;
		//printf("%f\n",obj_fcn[0]);
		//t_f_ini = wtime();
		temp_dist (tmp_dist, dist, c_numbers, rows, exponent, error);
		//t_f_end = wtime() - t_f_ini;
		//t_f_ini = wtime();
		memset(sum_col_dist,0,rows*sizeof(type_data));
		sum_col (sum_col_dist,tmp_dist,c_numbers,rows);
		//t_f_end = wtime() - t_f_ini;
		//t_f_ini = wtime();
		new_u_for_next_it (u,tmp_dist,sum_col_dist,c_numbers,rows);	
		//t_f_end = wtime() - t_f_ini;
    printf("Iteration count = %d, obj_fcn = %f\n", step, obj_fcn[step]); //QUITADO BALDO
		//if (step == 2) exit(0);
		//printf("\n\n");
    //if (step > 0) if (fabs(obj_fcn[step] - obj_fcn[step-1]) < error) break; //SI QUITAMOS ESTE COMENTARIO LAS EJECUCIONES PUEDEN SER QUE FINALICEN ANTES PORQUE HEMOS LLEGADO AL ERROR PERMITIDO.
		//if (step < (iterations - 1)) free(center);
		//exit(0);
  }
	free(mf); free(mf_t); free(den_center); free(den_center_t); free(numerator); free(dist); free(dist_2); free(tmp_dist); free(sum_col_dist);free(obj_fcn);

}

