# OS Name (Linux or Darwin)
OSUPPER = $(shell uname -s 2>/dev/null | tr [:lower:] [:upper:])
OSLOWER = $(shell uname -s 2>/dev/null | tr [:upper:] [:lower:])

# Flags to detect 32-bit or 64-bit OS platform
OS_SIZE = $(shell uname -m | sed -e "s/i.86/32/" -e "s/x86_64/64/")
OS_ARCH = $(shell uname -m | sed -e "s/i386/i686/")


# These flags will override any settings
ifeq ($(i386),1)
	OS_SIZE = 32
	OS_ARCH = i686
endif

ifeq ($(x86_64),1)
	OS_SIZE = 64
	OS_ARCH = x86_64
endif

# Flags to detect either a Linux system (linux) or Mac OSX (darwin)
DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))

# Location of the CUDA Toolkit binaries and libraries
CUDA_PATH       ?= /usr/local/cuda
CUDA_INC_PATH   ?= $(CUDA_PATH)/include
CUDA_BIN_PATH   ?= $(CUDA_PATH)/bin
ifneq ($(DARWIN),)
  CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
else
  ifeq ($(OS_SIZE),32)
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
  else
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib64
  endif
endif

# Common binaries
NVCC            ?= $(CUDA_BIN_PATH)/nvcc
GCC             ?= g++

EXTRA_NVCCFLAGS ?= -use_fast_math
EXTRA_LDFLAGS   ?=  

# CUDA code generation flags 37 50 52 60 61
GENCODE_SM35    := -gencode arch=compute_35,code=sm_35
GENCODE_SM37    := -gencode arch=compute_37,code=sm_37
GENCODE_FLAGS   := $(GENCODE_SM35) $(GENCODE_SM37) --cudart=shared
# OS-specific build flags
ifneq ($(DARWIN),) 
      LDFLAGS   := -Xlinker -rpath -L$(CUDA_LIB_PATH) -lstdc++ -fopenmp
      CCFLAGS   := -arch $(OS_ARCH) 
else
  ifeq ($(OS_SIZE),32)
      LDFLAGS   := -lstdc++ -fopenmp
      CCFLAGS   := -m32
  else
      LDFLAGS   := -L$(CUDA_LIB_PATH) -lcurand -lcublas -lcudart -use_fast_math -lstdc++ -fopenmp
      CCFLAGS   := -m64
  endif
endif

# OS-architecture specific flags
ifeq ($(OS_SIZE),32)
      NVCCFLAGS := -m32
else
      NVCCFLAGS := -m64
endif

# Debug build flags
ifeq ($(dbg),1)
      CCFLAGS   += 
      NVCCFLAGS += 
      TARGET    := debug
else
      CCFLAGS   += -O3 #-ffast-math #-march=native -std=c++17
      NVCCFLAGS += -O3
      TARGET    := release
endif


# Common includes and paths for CUDA
INCLUDES      := -I$(CUDA_INC_PATH)  

# Target rules
all: build

build: fcm

wtime.o: wtime.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<

fcm_common.o: fcm_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<  

fcm_distance.o: fcm_distance.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<

fcm_gpu.o: fcm_gpu.cu
	$(NVCC) $(CCFLAGS) $(GENCODE_FLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -o $@ -c -w -Wno-deprecated-gpu-targets -Xptxas -dlcm=ca -use_fast_math $<

fcm_cpu.o: fcm_cpu.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<

fcm_omp.o: fcm_omp.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<

main.o: main.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) -o $@ -c -w -fopenmp $<

fcm: main.o fcm_omp.o fcm_gpu.o wtime.o fcm_common.o fcm_distance.o fcm_cpu.o 
	$(GCC) $(CCFLAGS) -o $@ $+ $(LDFLAGS) $(EXTRA_LDFLAGS)

run: build
	./fcm

clean:
	rm -f fcm *.o log

