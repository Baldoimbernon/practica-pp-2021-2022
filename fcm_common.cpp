#include <omp.h>
#include <iostream>
#include <iomanip>
#include <cfloat>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fcm_common.h"
#include "fcm_distance.h"
#include "definitions.h"
#include "wtime.h"

using namespace std;

extern char *extract_name (char *filename)
{
	char *new_f = (char *)malloc(sizeof(char)*strlen(filename));
	int len = strlen(filename);
	int j = strlen(filename);
	int i=0;

	while ((filename[j] != '.') && (j > 0)) j--;
	j--;
	while ((filename[j] != '/') && (j > 0)) j--;
	if (j != 0) j++;
	while ((filename[j] != '.') && (j < len))
	{
		new_f[i] = filename[j];
		i++;
		j++;
	}
	new_f[i] = '\0';
	return new_f;
}

type_data round_up ( type_data value, type_data precision ) { 
    type_data pow_t = pow ( 10, precision ); 
    return ( ceil ( pow_t * value ) + ceil ( pow_t * value - ceil ( pow_t * value ) ) ) / pow_t; 
}

extern void create_static_u (type_data *&m)
{
	m = (type_data *)calloc(3*150,sizeof(type_data));
	m[0*3+0]=0.44099;m[0*3+1]=0.49028;m[0*3+2]=0.068734;
	m[1*3+0]=0.55583;m[1*3+1]=0.38482;m[1*3+2]=0.059357;
	m[2*3+0]=0.15621;m[2*3+1]=0.30674;m[2*3+2]=0.53705;
	m[3*3+0]=0.46099;m[3*3+1]=0.075301;m[3*3+2]=0.46371;
	m[4*3+0]=0.42677;m[4*3+1]=0.21641;m[4*3+2]=0.35682;
	m[5*3+0]=0.095909;m[5*3+1]=0.28509;m[5*3+2]=0.619;
	m[6*3+0]=0.32907;m[6*3+1]=0.39855;m[6*3+2]=0.27238;
	m[7*3+0]=0.019634;m[7*3+1]=0.46685;m[7*3+2]=0.51351;
	m[8*3+0]=0.3114;m[8*3+1]=0.34765;m[8*3+2]=0.34095;
	m[9*3+0]=0.32179;m[9*3+1]=0.53777;m[9*3+2]=0.14044;
	m[10*3+0]=0.69575;m[10*3+1]=0.031369;m[10*3+2]=0.27288;
	m[11*3+0]=0.047759;m[11*3+1]=0.10047;m[11*3+2]=0.85177;
	m[12*3+0]=0.35412;m[12*3+1]=0.16161;m[12*3+2]=0.48428;
	m[13*3+0]=0.0403;m[13*3+1]=0.5133;m[13*3+2]=0.4464;
	m[14*3+0]=0.43804;m[14*3+1]=0.45503;m[14*3+2]=0.10693;
	m[15*3+0]=0.30965;m[15*3+1]=0.28172;m[15*3+2]=0.40863;
	m[16*3+0]=0.40766;m[16*3+1]=0.43371;m[16*3+2]=0.15863;
	m[17*3+0]=0.45392;m[17*3+1]=0.43749;m[17*3+2]=0.1086;
	m[18*3+0]=0.075453;m[18*3+1]=0.316;m[18*3+2]=0.60855;
	m[19*3+0]=0.29613;m[19*3+1]=0.50917;m[19*3+2]=0.19471;
	m[20*3+0]=0.49676;m[20*3+1]=0.16868;m[20*3+2]=0.33456;
	m[21*3+0]=0.27423;m[21*3+1]=0.34947;m[21*3+2]=0.3763;
	m[22*3+0]=0.65524;m[22*3+1]=0.16599;m[22*3+2]=0.17877;
	m[23*3+0]=0.19039;m[23*3+1]=0.6216;m[23*3+2]=0.18801;
	m[24*3+0]=0.40979;m[24*3+1]=0.12255;m[24*3+2]=0.46765;
	m[25*3+0]=0.43876;m[25*3+1]=0.24646;m[25*3+2]=0.31477;
	m[26*3+0]=0.42751;m[26*3+1]=0.32845;m[26*3+2]=0.24404;
	m[27*3+0]=0.42264;m[27*3+1]=0.29772;m[27*3+2]=0.27964;
	m[28*3+0]=0.4679;m[28*3+1]=0.14582;m[28*3+2]=0.38628;
	m[29*3+0]=0.44285;m[29*3+1]=0.22353;m[29*3+2]=0.33362;
	m[30*3+0]=0.11483;m[30*3+1]=0.081668;m[30*3+2]=0.80351;
	m[31*3+0]=0.42275;m[31*3+1]=0.50677;m[31*3+2]=0.070483;
	m[32*3+0]=0.54168;m[32*3+1]=0.44699;m[32*3+2]=0.011334;
	m[33*3+0]=0.26061;m[33*3+1]=0.12537;m[33*3+2]=0.61402;
	m[34*3+0]=0.30954;m[34*3+1]=0.5257;m[34*3+2]=0.16476;
	m[35*3+0]=0.39629;m[35*3+1]=0.17312;m[35*3+2]=0.43059;
	m[36*3+0]=0.36507;m[36*3+1]=0.39629;m[36*3+2]=0.23865;
	m[37*3+0]=0.068362;m[37*3+1]=0.18675;m[37*3+2]=0.74489;
	m[38*3+0]=0.10048;m[38*3+1]=0.54454;m[38*3+2]=0.35498;
	m[39*3+0]=0.65665;m[39*3+1]=0.051533;m[39*3+2]=0.29181;
	m[40*3+0]=0.09938;m[40*3+1]=0.8963;m[40*3+2]=0.0043182;
	m[41*3+0]=0.31489;m[41*3+1]=0.33211;m[41*3+2]=0.353;
	m[42*3+0]=0.11348;m[42*3+1]=0.53728;m[42*3+2]=0.34925;
	m[43*3+0]=0.37349;m[43*3+1]=0.20139;m[43*3+2]=0.42511;
	m[44*3+0]=0.3076;m[44*3+1]=0.44622;m[44*3+2]=0.24618;
	m[45*3+0]=0.085844;m[45*3+1]=0.54843;m[45*3+2]=0.36573;
	m[46*3+0]=0.35524;m[46*3+1]=0.093649;m[46*3+2]=0.55111;
	m[47*3+0]=0.41854;m[47*3+1]=0.23613;m[47*3+2]=0.34533;
	m[48*3+0]=0.55986;m[48*3+1]=0.10585;m[48*3+2]=0.33429;
	m[49*3+0]=0.22537;m[49*3+1]=0.3361;m[49*3+2]=0.43853;
	m[50*3+0]=0.30466;m[50*3+1]=0.036254;m[50*3+2]=0.65909;
	m[51*3+0]=0.49082;m[51*3+1]=0.25501;m[51*3+2]=0.25417;
	m[52*3+0]=0.21015;m[52*3+1]=0.56008;m[52*3+2]=0.22977;
	m[53*3+0]=0.086796;m[53*3+1]=0.609;m[53*3+2]=0.3042;
	m[54*3+0]=0.3257;m[54*3+1]=0.54431;m[54*3+2]=0.12998;
	m[55*3+0]=0.065006;m[55*3+1]=0.46403;m[55*3+2]=0.47097;
	m[56*3+0]=0.66134;m[56*3+1]=0.06873;m[56*3+2]=0.26993;
	m[57*3+0]=0.29683;m[57*3+1]=0.69022;m[57*3+2]=0.012947;
	m[58*3+0]=0.049962;m[58*3+1]=0.19624;m[58*3+2]=0.7538;
	m[59*3+0]=0.39976;m[59*3+1]=0.35388;m[59*3+2]=0.24635;
	m[60*3+0]=0.34446;m[60*3+1]=0.1866;m[60*3+2]=0.46894;
	m[61*3+0]=0.17839;m[61*3+1]=0.64837;m[61*3+2]=0.17325;
	m[62*3+0]=0.20768;m[62*3+1]=0.35259;m[62*3+2]=0.43973;
	m[63*3+0]=0.045417;m[63*3+1]=0.52031;m[63*3+2]=0.43428;
	m[64*3+0]=0.35547;m[64*3+1]=0.31828;m[64*3+2]=0.32625;
	m[65*3+0]=0.2311;m[65*3+1]=0.3836;m[65*3+2]=0.3853;
	m[66*3+0]=0.3623;m[66*3+1]=0.3522;m[66*3+2]=0.2855;
	m[67*3+0]=0.21974;m[67*3+1]=0.47102;m[67*3+2]=0.30924;
	m[68*3+0]=0.16195;m[68*3+1]=0.43358;m[68*3+2]=0.40447;
	m[69*3+0]=0.31265;m[69*3+1]=0.35374;m[69*3+2]=0.33361;
	m[70*3+0]=0.212;m[70*3+1]=0.30742;m[70*3+2]=0.48058;
	m[71*3+0]=0.18155;m[71*3+1]=0.66504;m[71*3+2]=0.15341;
	m[72*3+0]=0.36188;m[72*3+1]=0.27344;m[72*3+2]=0.36467;
	m[73*3+0]=0.26087;m[73*3+1]=0.18627;m[73*3+2]=0.55286;
	m[74*3+0]=0.28305;m[74*3+1]=0.1216;m[74*3+2]=0.59535;
	m[75*3+0]=0.64047;m[75*3+1]=0.28689;m[75*3+2]=0.072639;
	m[76*3+0]=0.20454;m[76*3+1]=0.32395;m[76*3+2]=0.47151;
	m[77*3+0]=0.16635;m[77*3+1]=0.38245;m[77*3+2]=0.4512;
	m[78*3+0]=0.34875;m[78*3+1]=0.18467;m[78*3+2]=0.46659;
	m[79*3+0]=0.25486;m[79*3+1]=0.33912;m[79*3+2]=0.40603;
	m[80*3+0]=0.074425;m[80*3+1]=0.22844;m[80*3+2]=0.69713;
	m[81*3+0]=0.017306;m[81*3+1]=0.55014;m[81*3+2]=0.43256;
	m[82*3+0]=0.37458;m[82*3+1]=0.44351;m[82*3+2]=0.18191;
	m[83*3+0]=0.23307;m[83*3+1]=0.48919;m[83*3+2]=0.27774;
	m[84*3+0]=0.41972;m[84*3+1]=0.18652;m[84*3+2]=0.39376;
	m[85*3+0]=0.36737;m[85*3+1]=0.39979;m[85*3+2]=0.23283;
	m[86*3+0]=0.26374;m[86*3+1]=0.70917;m[86*3+2]=0.027089;
	m[87*3+0]=0.34115;m[87*3+1]=0.35199;m[87*3+2]=0.30686;
	m[88*3+0]=0.14184;m[88*3+1]=0.37628;m[88*3+2]=0.48188;
	m[89*3+0]=0.4421;m[89*3+1]=0.088815;m[89*3+2]=0.46909;
	m[90*3+0]=0.08509;m[90*3+1]=0.52105;m[90*3+2]=0.39386;
	m[91*3+0]=0.3249;m[91*3+1]=0.2982;m[91*3+2]=0.37689;
	m[92*3+0]=0.4631;m[92*3+1]=0.1737;m[92*3+2]=0.36321;
	m[93*3+0]=0.20342;m[93*3+1]=0.031407;m[93*3+2]=0.76517;
	m[94*3+0]=0.26531;m[94*3+1]=0.25465;m[94*3+2]=0.48004;
	m[95*3+0]=0.29223;m[95*3+1]=0.29596;m[95*3+2]=0.41181;
	m[96*3+0]=0.51465;m[96*3+1]=0.36848;m[96*3+2]=0.11687;
	m[97*3+0]=0.20771;m[97*3+1]=0.76746;m[97*3+2]=0.024824;
	m[98*3+0]=0.29936;m[98*3+1]=0.10261;m[98*3+2]=0.59803;
	m[99*3+0]=0.42315;m[99*3+1]=0.29715;m[99*3+2]=0.2797;
	m[100*3+0]=0.076042;m[100*3+1]=0.86984;m[100*3+2]=0.05412;
	m[101*3+0]=0.10357;m[101*3+1]=0.75621;m[101*3+2]=0.14022;
	m[102*3+0]=0.34695;m[102*3+1]=0.34669;m[102*3+2]=0.30636;
	m[103*3+0]=0.11284;m[103*3+1]=0.49667;m[103*3+2]=0.39049;
	m[104*3+0]=0.40167;m[104*3+1]=0.26792;m[104*3+2]=0.3304;
	m[105*3+0]=0.26515;m[105*3+1]=0.25264;m[105*3+2]=0.48222;
	m[106*3+0]=0.21401;m[106*3+1]=0.34144;m[106*3+2]=0.44455;
	m[107*3+0]=0.19299;m[107*3+1]=0.41042;m[107*3+2]=0.39659;
	m[108*3+0]=0.061292;m[108*3+1]=0.40468;m[108*3+2]=0.53403;
	m[109*3+0]=0.24494;m[109*3+1]=0.38602;m[109*3+2]=0.36904;
	m[110*3+0]=0.39504;m[110*3+1]=0.58401;m[110*3+2]=0.020953;
	m[111*3+0]=0.78259;m[111*3+1]=0.13294;m[111*3+2]=0.08447;
	m[112*3+0]=0.35126;m[112*3+1]=0.18687;m[112*3+2]=0.46188;
	m[113*3+0]=0.15352;m[113*3+1]=0.43032;m[113*3+2]=0.41617;
	m[114*3+0]=0.049711;m[114*3+1]=0.69632;m[114*3+2]=0.25397;
	m[115*3+0]=0.22098;m[115*3+1]=0.28633;m[115*3+2]=0.49269;
	m[116*3+0]=0.24541;m[116*3+1]=0.5775;m[116*3+2]=0.17709;
	m[117*3+0]=0.36773;m[117*3+1]=0.3495;m[117*3+2]=0.28277;
	m[118*3+0]=0.4525;m[118*3+1]=0.43203;m[118*3+2]=0.11546;
	m[119*3+0]=0.098608;m[119*3+1]=0.76958;m[119*3+2]=0.13181;
	m[120*3+0]=0.022092;m[120*3+1]=0.3803;m[120*3+2]=0.59761;
	m[121*3+0]=0.5447;m[121*3+1]=0.15501;m[121*3+2]=0.30029;
	m[122*3+0]=0.28818;m[122*3+1]=0.614;m[122*3+2]=0.097828;
	m[123*3+0]=0.4559;m[123*3+1]=0.34359;m[123*3+2]=0.20051;
	m[124*3+0]=0.17338;m[124*3+1]=0.3889;m[124*3+2]=0.43772;
	m[125*3+0]=0.12882;m[125*3+1]=0.62961;m[125*3+2]=0.24157;
	m[126*3+0]=0.31541;m[126*3+1]=0.47809;m[126*3+2]=0.2065;
	m[127*3+0]=0.24764;m[127*3+1]=0.52616;m[127*3+2]=0.22619;
	m[128*3+0]=0.3249;m[128*3+1]=0.38729;m[128*3+2]=0.28781;
	m[129*3+0]=0.33202;m[129*3+1]=0.56393;m[129*3+2]=0.10405;
	m[130*3+0]=0.34808;m[130*3+1]=0.33784;m[130*3+2]=0.31407;
	m[131*3+0]=0.29709;m[131*3+1]=0.67725;m[131*3+2]=0.025653;
	m[132*3+0]=0.47279;m[132*3+1]=0.34767;m[132*3+2]=0.17953;
	m[133*3+0]=0.25689;m[133*3+1]=0.6077;m[133*3+2]=0.13541;
	m[134*3+0]=0.33903;m[134*3+1]=0.26675;m[134*3+2]=0.39422;
	m[135*3+0]=0.51011;m[135*3+1]=0.46539;m[135*3+2]=0.024492;
	m[136*3+0]=0.074849;m[136*3+1]=0.34767;m[136*3+2]=0.57748;
	m[137*3+0]=0.34773;m[137*3+1]=0.21658;m[137*3+2]=0.43569;
	m[138*3+0]=0.32383;m[138*3+1]=0.43665;m[138*3+2]=0.23952;
	m[139*3+0]=0.31212;m[139*3+1]=0.1014;m[139*3+2]=0.58648;
	m[140*3+0]=0.60229;m[140*3+1]=0.32748;m[140*3+2]=0.070238;
	m[141*3+0]=0.38006;m[141*3+1]=0.21915;m[141*3+2]=0.40079;
	m[142*3+0]=0.30891;m[142*3+1]=0.37002;m[142*3+2]=0.32108;
	m[143*3+0]=0.37456;m[143*3+1]=0.22167;m[143*3+2]=0.40377;
	m[144*3+0]=0.34732;m[144*3+1]=0.52159;m[144*3+2]=0.1311;
	m[145*3+0]=0.41303;m[145*3+1]=0.17658;m[145*3+2]=0.41039;
	m[146*3+0]=0.6829;m[146*3+1]=0.066796;m[146*3+2]=0.2503;
	m[147*3+0]=0.12904;m[147*3+1]=0.38464;m[147*3+2]=0.48633;
	m[148*3+0]=0.19133;m[148*3+1]=0.43355;m[148*3+2]=0.37512;
	m[149*3+0]=0.0067446;m[149*3+1]=0.6048;m[149*3+2]=0.38846;

	//return (m);

}

extern void create_u (unsigned int c_numbers,unsigned int rows, type_data *&m, type_data  exponent)
{
	double time_init,time_end;
	unsigned int i,j;
	type_data total = 0, prob_total = 0;
	type_data *sum_col_u = (type_data *)calloc(rows,sizeof(type_data));
	m = (type_data *)calloc(c_numbers*rows,sizeof(type_data));
	srand(time(NULL));
	time_init = wtime();

	for(i=0; i < rows; i++)
	{
		for(j=0; j < c_numbers; j++)
			m[i*c_numbers+j] = (type_data)rand();  
	}
	time_end = wtime() - time_init;
	printf("\nTime: %f seg\n",time_end);
	for(i=0; i < rows; i++)
		for (unsigned int j=0; j < c_numbers; j++) 
			sum_col_u[i] += m[i*c_numbers+j];

	 for(i=0; i < rows; i++)
                for (j=0; j < c_numbers; j++)
			m[i*c_numbers+j] /= sum_col_u[i];
}

extern type_data *create_obj (type_data *obj_fcn, unsigned int iterations)
{
	obj_fcn = (type_data *)calloc(iterations,sizeof(type_data));
	return obj_fcn;
}

extern void show_data (type_data *m, unsigned int rows, unsigned int columns)
{
	for (unsigned int i=0; i < rows;i++)
	{
		for(unsigned int j=0; j < columns; j++)
			printf("%lf ",m[i*columns+j]);
		printf ("\n");
	}
	printf("\n\n");
}

extern void show_data_file (type_data *m, type_data *c, unsigned int c_numbers, unsigned int rows, unsigned int columns, char *output)
{
	int cl,i,j;
	float max_prob;
	char temp_cl[200];
	char temp_cen[200];
	char temp_gen[200];

	char *output_cl = (char *)malloc(sizeof(char)*(strlen(output)+4));
	char *output_cen = (char *)malloc(sizeof(char)*(strlen(output)+10));
	char *output_gen = (char *)malloc(sizeof(char)*(strlen(output)+10));

	char *base_name = extract_name (output);

	strcpy(output_gen,base_name);
	strcpy(output_cl,base_name);
	strcpy(output_cen,base_name);

	sprintf(temp_gen,"_%d_probability",c_numbers);
	sprintf(temp_cl,"_%d_clusters",c_numbers);
	sprintf(temp_cen,"_%d_centers",c_numbers);
	strcat(output_gen,temp_gen);
	strcat(output_cl,temp_cl);
        strcat(output_cen,temp_cen);

	ofstream fp(output_gen, ios::out | ios::trunc);
	ofstream fp_cl(output_cl, ios::out | ios::trunc);
	ofstream fp_cen(output_cen, ios::out | ios::trunc);

        if (!fp.good()) {
                cout << "can't open file " << output_gen << endl;
                cout << "Will not create file" << endl;
                exit (1);
        }
	if (!fp_cl.good()) {
                cout << "can't open file " << output_cl << endl;
                cout << "Will not create file" << endl;
                exit (1);
        }
	if (!fp_cen.good()) {
                cout << "can't open file " << output_cen << endl;
                cout << "Will not create file" << endl;
                exit (1);
        }
	
	//printf("%d %d %d\n",c_numbers,rows,columns);
        for (i=0; i < rows;i++)
        {
		max_prob = 0;
		cl = 0;
                for(j=0; j < c_numbers; j++)
		{
			if (max_prob < m[i*c_numbers+j]) 
			{ 
				cl = j; max_prob = m[i*c_numbers+j];
			}
                        fp << fixed << setprecision(6) << m[i*c_numbers+j] << " ";
		}
		fp_cl << cl << endl;	
                fp << endl;
        }
	for (i=0; i < c_numbers; i++)
	{
                for (j=0; j < columns; j++)
		{
			fp_cen << fixed << setprecision(6) << c[i*columns+j];
			if (j < (columns-1)) fp_cen << ",";
		}
		fp_cen << endl;
	}
        fp.close(); 
	fp_cl.close();
	fp_cen.close();
}


type_data extract_number (char *buffer, unsigned int *pos, char separator)
{	unsigned int k = 0;
	char c;
	c = buffer[*pos];
	char temp[100];
	//type_data number;
	while (buffer[*pos] == ' ') (*pos)++;
        c = buffer[*pos];
	while (c != separator)
	{
		temp[k] = buffer[*pos];
		k++;(*pos)++;
		c = buffer[*pos];
	}	
	(*pos)++;
	temp[k]='\0';
	if (c == '\n') temp[k-1] = '\0';
	return ((type_data)atof(temp));
}

unsigned int extract_columns (char *buffer, char separator)
{
        unsigned int c_t = 1, i = 0;
        unsigned int len = strlen(buffer);
	 while (buffer[i] == separator) i++;
        char c = buffer[i];
        while ((c != '\0') && (i < len))
        {
                if (c == separator)
                {
                        while (c == separator)
                        {
                                i++;
                                c = buffer[i];
                        }
                        c_t++;
                }
                else
                {
                        i++;
                        c = buffer[i];
                }
        }
        return c_t;
}

extern void exam_data (char *input, unsigned int *rows, unsigned int *columns, char separator) {

        unsigned int r_t = 1;
        char buffer[MAX_LINE+1];// = (char *)malloc(sizeof(char)*MAX_LINE);

        FILE *fp;
        if ((fp =  fopen(input,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", input);
                exit(1);
        }
	fgets(buffer,MAX_LINE,fp);
	*columns = extract_columns(buffer,separator);
	
	while(fgets(buffer,MAX_LINE,fp) != NULL ) r_t++;
        *rows = r_t;
	printf("%u %u %u\n",*rows,*columns,*rows * *columns);
	//exit(0);
	 fclose(fp);	
}

extern void multiply_matrix_one_one (type_data *center, type_data *numerator, type_data *den_center_t, unsigned int c_numbers, unsigned int rows, unsigned int columns)
{
	unsigned int i,j;
	for (i=0; i < c_numbers; i++)
                for (j=0; j < columns; j++)
                        center[i*columns+j] = numerator[i*columns+j] / den_center_t[i*columns+j];
		
}
extern void u_up (type_data *u_2_ele, type_data *u, unsigned int rows, unsigned int columns, type_data exponent)
{
	unsigned int i,j;
	for (i = 0; i < rows; i++)
		for (j = 0; j < columns; j++)
			u_2_ele[i*columns+j] = pow(u[i*columns+j],exponent);
}

extern void transposed (type_data *u_t, type_data *u, unsigned int rows, unsigned int columns)
{
	unsigned int i,j;
	for (i=0; i < rows; i++)
		for (j=0; j < columns;j++)
			u_t[j*rows+i] = u[i*columns+j];
}

extern void denominator_center(type_data *den_center, type_data *u_2_ele, unsigned int rows, unsigned int columns, unsigned int c_numbers)
{
	unsigned int i,j;
	type_data *sum_col_u_2_ele = (type_data *)calloc(c_numbers,sizeof(type_data));
	for (i=0; i < c_numbers; i++) 
	{	
		float dot = 0;
		for (j=0; j < rows; j++)	
			dot += u_2_ele[j*c_numbers+i];
		sum_col_u_2_ele[i] = dot; 
	}
	
	for (i=0; i < columns; i++) 
                for (j=0; j < c_numbers; j++)
			den_center[i*c_numbers+j] = sum_col_u_2_ele[j];
}

extern void multiply (type_data *numerator, type_data *mf, type_data *data_matrix, unsigned int c_numbers, unsigned int rows, unsigned int columns)
{
	unsigned int i,j,k;
	for(i = 0; i < c_numbers ;i++)
      		for(j = 0; j < columns ;j++)
		{
			type_data dot = 0;
          		for(k = 0; k < rows ;k++)
              			dot+=(mf[i*rows+k]*data_matrix[k*columns+j]);
			numerator[i*columns+j] = dot;
		}
}

extern void distance_matrix (type_data *dist,type_data *center, type_data *data_matrix, unsigned int rows, unsigned int columns, unsigned int c_numbers)
{
	unsigned int i,j;
	for (i=0; i < c_numbers; i++)
	{
		for (j=0; j < rows; j++)
			dist[i*rows+j] = euclidean_distance((center + i*columns),(data_matrix + j*columns),columns,i,j);
	}
}

extern type_data multiply_one_one(type_data *dist_2, type_data *mf,unsigned int c_numbers,unsigned int rows)
{
	unsigned int i,j;
	type_data total_temp = 0, total_temp_dist_2_mf = 0;
	for (i=0; i < c_numbers; i++)
		for (j=0; j < rows; j++)		
			total_temp_dist_2_mf +=  dist_2[i*rows+j] * mf[i*rows+j];
	return total_temp_dist_2_mf;
}

extern void temp_dist (type_data *tmp_dist, type_data *dist, unsigned int c_numbers, unsigned int rows, type_data exponent, type_data error)	
{
	type_data temp_exponent = 0.0f;
	type_data temp_pow = 0.0f;
	unsigned int i,j;
	if (exponent != 1)
	{
		temp_exponent = -2/(exponent-1);
		for (i=0; i < c_numbers; i++) 	
			for (j=0; j < rows; j++)		
				if ((dist[i*rows+j] < (error/10)) && (temp_exponent < 0)) tmp_dist[i*rows+j] = FLT_MAX; else tmp_dist[i*rows+j] = (type_data) pow(dist[i*rows+j],temp_exponent);
	}	
}

extern void sum_col (type_data *sum_col_dist, type_data *tmp_dist, unsigned int c_numbers, unsigned int rows)
{
	unsigned int i,j;
	type_data dot;
	for (i=0; i < c_numbers; i++) 
	{
	    	for (j=0; j < rows; j++)
			sum_col_dist[j] += tmp_dist[i*rows+j];			
	}
}

extern void new_u_for_next_it (type_data *&u, type_data *tmp, type_data *sum_col_dist, unsigned int c_numbers, unsigned int rows) 
{
	unsigned int i,j;
	free(u);
	u = (type_data *)malloc(sizeof(type_data)*c_numbers*rows);

	for (i = 0; i < c_numbers; i++){
		for (j = 0; j < rows; j++){
			u[i*rows+j] = tmp[i*rows+j] / sum_col_dist[j];
		}
	}
}

