#ifndef _FCM_DISTANCE_H_
#define _FCM_DISTANCE_H_
#include "definitions.h"

type_data euclidean_distance (type_data *, type_data *, unsigned int, int, int );

#endif
