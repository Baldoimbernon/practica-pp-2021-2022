#ifndef _FCM_GPU_
#define _FCM_GPU_

#include <stdio.h>

extern void fcm_calculation_GPU (type_data *data_matrix,type_data *u, type_data *&u_host, type_data *&center,unsigned int rows,unsigned int columns,unsigned int c_numbers, type_data exponent,type_data error,unsigned int mode,unsigned int iterations, int ngpu);
extern type_data *read_data (char *input, unsigned int rows, unsigned int columns, type_data *m, char separator);

#endif
